const readFile = require('./src/readFile');
const dayOne = require('./src/day1');
const dayTwo = require('./src/day2');
const dayThree = require('./src/day3');


function noop(inputs) {}

function fns(day) {
    switch (Number(day)) {
        case  1: return { partOne: dayOne.partOne,   partTwo: dayOne.partTwo };
        case  2: return { partOne: dayTwo.partOne,   partTwo: dayTwo.partTwo };
        case  3: return { partOne: dayThree.partOne, partTwo: dayThree.partTwo };
        case  4: return { partOne: noop, partTwo: noop };
        case  5: return { partOne: noop, partTwo: noop };
        case  6: return { partOne: noop, partTwo: noop };
        case  7: return { partOne: noop, partTwo: noop };
        case  8: return { partOne: noop, partTwo: noop };
        case  9: return { partOne: noop, partTwo: noop };
        case 10: return { partOne: noop, partTwo: noop };
        case 11: return { partOne: noop, partTwo: noop };
        case 12: return { partOne: noop, partTwo: noop };
        case 13: return { partOne: noop, partTwo: noop };
        case 14: return { partOne: noop, partTwo: noop };
        case 15: return { partOne: noop, partTwo: noop };
        case 16: return { partOne: noop, partTwo: noop };
        case 17: return { partOne: noop, partTwo: noop };
        case 18: return { partOne: noop, partTwo: noop };
        case 19: return { partOne: noop, partTwo: noop };
        case 20: return { partOne: noop, partTwo: noop };
        case 21: return { partOne: noop, partTwo: noop };
        case 22: return { partOne: noop, partTwo: noop };
        case 23: return { partOne: noop, partTwo: noop };
        case 24: return { partOne: noop, partTwo: noop };
        case 25: return { partOne: noop, partTwo: noop };
        default: return { partOne: noop, partTwo: noop };
    }
}

const requestedDay = process.argv[2];
const fileData = readFile(`inputs/day${requestedDay}.txt`);
const { partOne, partTwo } = fns(requestedDay);

const [ansOne, ansTwo] = [partOne(fileData), partTwo(fileData)];
console.log(`The answer for day ${requestedDay} part 1: ${ansOne}`);
console.log(`The answer for day ${requestedDay} part 2: ${ansTwo}`);
