const fs = require('fs');

function trimTrailingWhitespace(line) {
    return line.trimRight();
}

function removeEmptyLines(line) {
    return line.length > 0;
}

// read the input file and return an array of Strings where
// each element in the array is a line from the file
function readFile(path) {
    try {
        return fs.readFileSync(path, "utf-8")
            .split('\n')
            .map(trimTrailingWhitespace)
            .filter(removeEmptyLines);
    } catch {
        return [];
    }
}

module.exports = readFile;
