// Get product of the forward position and depth after following the planned course.

function parseToNumber(inputLine){
    return Number(inputLine.split(' ')[1]);
}

function parseToDirection(inputLine){
    return inputLine.split(' ')[0];
}

function calculateForwardMovement(inputDirections){
    let sum = 0;

    inputDirections.forEach(line => {
        if(parseToDirection(line) === 'forward'){
            sum += parseToNumber(line);
        }
    });

    return sum;
}

function calculateDepth(inputDirections){
    let sum = 0;

    inputDirections.forEach(line => {
        const direction = parseToDirection(line);

        if (direction === 'down'){
            sum += parseToNumber(line);
        }else if (direction === 'up') {
            sum -= parseToNumber(line);
        }
    });

    return sum;
}

function calculateProduct(input){

    let depth = 0;
    let forwardMovement = 0;

    input.forEach(line => {
        const direction = parseToDirection(line);
        const value = parseToNumber(line);

        switch (direction){
            case 'forward':
                forwardMovement += value;
                break;
            case 'up':
                depth -= value;
                break;
            case 'down':
                depth += value;
                break;

            default:
                break;
        }

    });

    return depth * forwardMovement;
}

function calculateAimProduct(input){

    let depth = 0;
    let forwardMovement = 0;
    let aim = 0;

    input.forEach(line => {
        const direction = parseToDirection(line);
        const value = parseToNumber(line);

        switch (direction){
            case 'forward':
                forwardMovement += value;
                depth += value * aim;
                break;
            case 'up':
                aim -= value;
                break;
            case 'down':
                aim += value;
                break;

            default:
                break;
        }

    });

    return depth * forwardMovement;
}


module.exports = {
    parseToNumber,
    parseToDirection,
    calculateForwardMovement,
    calculateDepth,
    calculateProduct,
    calculateAimProduct
}