const {
    parseToNumber,
    parseToDirection,
    calculateForwardMovement,
    calculateDepth,
    calculateProduct,
    calculateAimProduct
} = require('./day2');

describe('dayTwo', () => {
    const sampleInput = [
        'forward 5',
        'down 5',
        'forward 8',
        'up 3',
        'down 8',
        'forward 2',
    ];

    const sampleLine = 'forward 5';

    test('function will parse the sampleInput into a Number', () => {
        expect(parseToNumber(sampleLine)).toEqual(5);
    });

    test('parse will parse the sampleInput into a direction', () => {
        expect(parseToDirection(sampleLine)).toEqual('forward');
    });

    test('calculate sum of forward movement', () => {
        expect(calculateForwardMovement(sampleInput)).toEqual(15);
    });

    test('calculate depth', () => {
        expect(calculateDepth(sampleInput)).toEqual(10);
    });

    test ('calculate product of depth and forward movement', () => {
        expect(calculateProduct(sampleInput)).toEqual((150));
    });

    test ('calculate aim product of depth and forward movement', () => {
        expect(calculateAimProduct(sampleInput)).toEqual((900));
    });

});
