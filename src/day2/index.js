const { calculateProduct: partOne, calculateAimProduct: partTwo } = require('./day2');

module.exports = {
    partOne,
    partTwo,
};