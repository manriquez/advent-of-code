// Count the number of times a depth measurement
// increases from the previous measurement


function parseToNumbers(input) {
    return input.map(Number);
}

function increaseDepth(input){
    const depths = parseToNumbers(input);

    let sum = 0;
    for(let i = 1; i < input.length; i++){
        if (depths[i] > depths[i-1]){
            sum++;
        }
    }

    return sum;
}


function increaseDepthWindow(input){
    const depths = parseToNumbers(input);

    let sum = 0;
    for(let i = 3; i < input.length; i++){

        const windowOne = depths[i - 3] + depths[i - 2] + depths[i - 1] ;
        const windowTwo = depths[i - 2] + depths[i - 1] + depths[i];

        if (windowTwo > windowOne){
            sum++;
        }
    }

    return sum;
}

module.exports = {
    parseToNumbers,
    increaseDepth,
    increaseDepthWindow,
};