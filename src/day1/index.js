const { increaseDepth: partOne, increaseDepthWindow: partTwo } = require('./day1');

module.exports = {
    partOne,
    partTwo,
};