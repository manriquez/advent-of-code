const { increaseDepth, increaseDepthWindow, parseToNumbers } = require('./day1');

describe('dayOne', () => {
    const sampleInput = [
        '199',
        '200',
        '208',
        '210',
        '200',
        '207',
        '240',
        '269',
        '260',
        '263'
    ];
    test('parse will parse the sampleInput into a Number', () => {
        expect(parseToNumbers(sampleInput)).toEqual([
            199, 200, 208, 210, 200, 207, 240, 269, 260, 263
        ]);
    });

    test('increaseDepth returns the elements that are greater than the previous element', () => {
        expect(increaseDepth(sampleInput)).toBe(7);
    });

    test('increaseDepthWindow sums 3 and returns the number of elements that are greater than the previous', () => {
        expect(increaseDepthWindow(sampleInput)).toBe(5);
    });
});
