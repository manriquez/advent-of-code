const { calculatePowerConsumption: partOne, calculateAnswer: partTwo } = require('./day3');

module.exports = {
    partOne,
    partTwo,
};