// Calculate the gamma rate and epsilon rate, then multiply them together to get the power consumption

function extractCharAtLocation(sampleLine, index) {
    return sampleLine.charAt(index);
}

function extractNumberAtLocation(sampleLine, index){
    return Number(extractCharAtLocation(sampleLine, index));
}

function getMostCommonNumber(sampleInput, index) {

    let zeroCount = 0;
    let oneCount  = 0;

    sampleInput.forEach(line => {
        if (extractCharAtLocation(line, index) === '1'){
            oneCount++;
        }else{
            zeroCount++;
        }
    });

    return (oneCount >= zeroCount) ? 1 : 0;
}


function countElements(arr, value){
    return arr.filter(x => x === value).length;
}

function getLeastCommonNumber(sampleInput, index) {

    let zeroCount = 0;
    let oneCount  = 0;

    sampleInput.forEach(line => {
        if (extractCharAtLocation(line, index) === '1'){
            oneCount++;
        }if (extractCharAtLocation(line, index) === '0'){
            zeroCount++;
        }
    });

    if(zeroCount === 0){
        return 1;
    }

    if(oneCount === 1){
        return 0;
    }

    if(oneCount >= zeroCount){
        return (zeroCount === 0) ? 1 : 0;
    }

    return (oneCount === 0) ? 0 : 1;
}


function filterMostCommon(input, index){
    if(input.length === 1){
        return input;
    }

    const mostCommonNumber = getMostCommonNumber(input, index)

    return input.filter(line => {
        return extractNumberAtLocation(line, index) === mostCommonNumber;
    });
}


function filterLeastCommon(input, index){

    if(input.length === 1){
        return input;
    }

    const leastCommonNumber = getLeastCommonNumber(input, index)

    return input.filter(line => {
        return extractNumberAtLocation(line, index) === leastCommonNumber;
    });

}


function columnsCount(input) {

    let columns = new Array(input[0].length).fill(0);

    input.forEach(line => {
        for(var i = 0; i < line.length; i++){
            if (extractCharAtLocation(line, i) === '1'){
                columns[i] = columns[i] + 1;
            }
        }
    });

    return columns;
}

function convertArrayToBinary(sampleArray, comparisonSize){
    let midPoint = comparisonSize / 2;
    let binaryArray = sampleArray.map(function (val) {
        return (val > midPoint) ? 1 : 0;
    })

    return binaryArray.join("");
}

function convertBinaryStringToNumber(binaryString) {
    return parseInt(binaryString, 2)
}

function invertBinaryString(binaryString){
    return binaryString.split("").map(val => {
        if (val === '1'){
            return '0';
        }

        return '1';
    }).join("")
}

function calculatePowerConsumption(sampleInput){

    const columns = columnsCount(sampleInput);
    const binaryString = convertArrayToBinary(columns, sampleInput.length);

    const gammaRate = convertBinaryStringToNumber(binaryString);
    const epsilonRate = convertBinaryStringToNumber(invertBinaryString(binaryString));

    return gammaRate * epsilonRate;
}

function calculateOxygenGeneratorRating(sampleInput) {

    const iterationCount = sampleInput[0].length;
    let filteredSampleInput = sampleInput;

    for(var i = 0; i < iterationCount; i++  ){
        filteredSampleInput = filterMostCommon(filteredSampleInput, i);
    }

    binaryString = filteredSampleInput.join('');
    return convertBinaryStringToNumber(binaryString);
}


function calculateCarbonDioxideScrubberRating(sampleInput) {
    const iterationCount = sampleInput[0].length;
    let filteredSampleInput = sampleInput;

    for(var i = 0; i < iterationCount; i++  ){
        filteredSampleInput = filterLeastCommon(filteredSampleInput, i);
    }

    binaryString = filteredSampleInput.join('');
    return convertBinaryStringToNumber(binaryString);
}


function calculateAnswer(input){
    const oxygenRating = calculateOxygenGeneratorRating(input);
    const carbonRating = calculateCarbonDioxideScrubberRating(input);

    return oxygenRating * carbonRating;
}



module.exports = {
    extractNumberAtLocation,
    columnsCount,
    convertArrayToBinary,
    convertBinaryStringToNumber,
    invertBinaryString,
    calculatePowerConsumption,
    getMostCommonNumber,
    filterMostCommon,
    calculateOxygenGeneratorRating,
    getLeastCommonNumber,
    filterLeastCommon,
    calculateCarbonDioxideScrubberRating,
    calculateAnswer
}