const {
    extractNumberAtLocation,
    columnsCount,
    convertArrayToBinary,
    convertBinaryStringToNumber,
    invertBinaryString,
    calculatePowerConsumption,
    getMostCommonNumber,
    filterMostCommon,
    calculateOxygenGeneratorRating,
    getLeastCommonNumber,
    filterLeastCommon,
    calculateCarbonDioxideScrubberRating,
    calculateAnswer
} = require('./day3');


describe('dayThree', () => {

    const sampleArray = [ 7, 5, 8, 7, 5 ];
    const sampleLine = '00100';
    const sampleInput = [
        '00100',
        '11110',
        '10110',
        '10111',
        '10101',
        '01111',
        '00111',
        '11100',
        '10000',
        '11001',
        '00010',
        '01010',
    ];

    const actualArray = [ 4, 0, 2, 3, 2, 2, 3, 4, 2, 0, 3, 2 ];
    const actualInput = [
        '101010011010',
        '101111111001',
        '100100100000',
        '000001010010',
        '100100110011',
        '000101100110'
    ];



    test('given the index, parse the number in the position of the string', () => {
        expect(extractNumberAtLocation(sampleLine, 0)).toEqual(0);
        expect(extractNumberAtLocation(sampleLine, 1)).toEqual(0);
        expect(extractNumberAtLocation(sampleLine, 2)).toEqual(1);
        expect(extractNumberAtLocation(sampleLine, 3)).toEqual(0);
        expect(extractNumberAtLocation(sampleLine, 4)).toEqual(0);
    })

    test('return the sums of each column', () => {
        expect(columnsCount(sampleInput)).toEqual([
            7, 5, 8, 7, 5
        ]);

        expect(columnsCount(actualInput)).toEqual([
            4, 0, 2, 4, 2, 3, 4, 4, 2, 1, 4, 2
        ]);
    })


    test('given an array of sums, return a binary number', () => {
       expect(convertArrayToBinary(sampleArray, 12)).toEqual('10110')
       expect(convertArrayToBinary(actualArray, 5)).toEqual('100100110010')
    })

    test('convert binary string to number', () => {
       expect(convertBinaryStringToNumber('10110')).toEqual(22);
       expect(convertBinaryStringToNumber('100100110010')).toEqual(2354);
    })

    test('invert binary string', () => {
        expect(invertBinaryString('100100110010')).toEqual('011011001101')
    })

    test('get power consumption', () =>{
        expect(calculatePowerConsumption(sampleInput)).toEqual(198);
        expect(calculatePowerConsumption(actualInput)).toEqual(4098314);
    })

    test('get the most number at index', () => {
        expect(getMostCommonNumber(sampleInput, 0)).toEqual(1)
    })

    test('filter array by most common number', () => {
        expect(filterMostCommon(sampleInput, 0)).toEqual([
            '11110',
            '10110',
            '10111',
            '10101',
            '11100',
            '10000',
            '11001',
        ])

        expect(filterMostCommon(actualInput, 0)).toEqual([
            '101010011010',
            '101111111001',
            '100100100000',
            '100100110011',
        ])
    })

    test('Calculate oxygen generator rating', () =>{
        expect(calculateOxygenGeneratorRating(sampleInput)).toEqual(23)
        expect(calculateOxygenGeneratorRating(actualInput)).toEqual(3065)
    })

    test('get the least number at index', () => {
        expect(getLeastCommonNumber(sampleInput, 0)).toEqual(0)
    })

    test('filter array by least common number', () => {
        expect(filterLeastCommon(sampleInput, 0)).toEqual([
            '00100',
            '01111',
            '00111',
            '00010',
            '01010',
        ])

        expect(filterLeastCommon(actualInput, 0)).toEqual([
            '000001010010',
            '000101100110',
        ])

        expect(filterLeastCommon(actualInput, 1)).toEqual([
            '101010011010',
            '101111111001',
            '100100100000',
            '000001010010',
            '100100110011',
            '000101100110',
        ])
    })

    test('Calculate carbon dioxide scrubber rating', () =>{
        expect(calculateCarbonDioxideScrubberRating(sampleInput)).toEqual(10)
        expect(calculateCarbonDioxideScrubberRating(actualInput)).toEqual(82)
    })

    test('Calculate product of the oxygen generator rating and CO2 scrubber rating', () => {
        expect(calculateAnswer(sampleInput)).toEqual(230)
        expect(calculateAnswer(actualInput)).toEqual(251330)
    })

});