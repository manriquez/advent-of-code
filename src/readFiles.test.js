const readFile = require('./readFile');

describe('readFile', () => {
    test('an empty file returns an empty array', () => {
        const input = readFile('emptyFile.txt');
        expect(input).toEqual([]);
    });

    test('a file returns all lines', () => {
        const input = readFile('inputs/test.txt');
        expect(input.length).toBe(10);
        expect(input).toEqual(['1','2','3','4','5','6','7','8','9','10']);
    });
});

